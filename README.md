# TextAPI Viewer
![pipeline badge](https://gitlab.gwdg.de/subugoe/emo/viewer/badges/master/pipeline.svg)

## Demo
https://subugoe.pages.gwdg.de/emo/viewer/

## Latest version
To embed the viewer in production the latest compiled and minified version is
available at https://gitlab.gwdg.de/subugoe/emo/viewer/-/jobs/artifacts/master/download?job=production

## Integration
To include the viewer on a website add the following to the html file:
```html
<noscript>
  <strong>We're sorry but TextViewer doesn't work properly without JavaScript
    enabled. Please enable it to continue.</strong>
</noscript>
<script id="emo-config" type="application/json">
{
  "manifest": "api/manifest.json"
}
</script>
<div id="viewer"></div>
<script src=js/chunk-vendors.[CHECKSUM].js></script>
<script src=js/app.[CHECKSUM].js></script>
```

and replace `[CHECKSUM]` with the values from the release you are going to use.

## Development

### Project setup
```
npm install
```

#### Compiles and hot-reloads for development
```
npm run serve
```

#### Compiles and minifies for production
```
npm run build
```

#### Run your tests
```
npm run test
```

#### Lints and fixes files
```
npm run lint
```
