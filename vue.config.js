module.exports = {
  lintOnSave: true,
  publicPath: './',

  pluginOptions: {
    quasar: {
      importStrategy: 'kebab',
      rtlSupport: false,
    },
  },

  transpileDependencies: [
    'quasar',
  ],
};
