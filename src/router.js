import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.NODE_ENV === 'development' ? '/' : '/emo/viewer/',
  routes: [
    {
      path: '/',
      name: 'TwinView',
      component: () => import('@/views/quasar-twinview.vue'),
    },
    {
      path: '/text',
      name: 'TextView',
      component: () => import('@/views/quasar-textview.vue'),
    },
    {
      path: '/image',
      name: 'ImageView',
      component: () => import('@/views/quasar-imageview.vue'),
    },
  ],
});
