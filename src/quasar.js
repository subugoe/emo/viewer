import Vue from 'vue';

import { Quasar } from 'quasar';

Vue.use(Quasar, {
  framework: {
    components: [
      'QHeader',
      'QLayout',
      'QPageContainer',
      'QPage',
      // 'QRouteTab',
      'QSplitter',
      // 'QTabs',
      'QToolbar',
    ],
  },
  plugins: {
    // Notify,
  },
});

export default { Quasar };
