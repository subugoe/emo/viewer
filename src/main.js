import Vue from 'vue';

import quasar from './quasar';

import router from './router';

import EmoViewer from './EmoViewer.vue';

Vue.config.productionTip = false;

new Vue({
  quasar,
  router,
  render: h => h(EmoViewer),
}).$mount('#app');
